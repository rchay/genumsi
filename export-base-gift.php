<?php
    session_start();

    if (empty($_SESSION) or $_SESSION['connecte'] != true) :
        echo "Vous ne devriez pas être ici : <a href='index.php'>Retour</a>";
    else :

        include('connexionbdd.php');

        $texte_req = 'SELECT *
        FROM questions
        INNER JOIN domaines ON questions.num_domaine = domaines.num_domaine
        INNER JOIN sous_domaines ON questions.num_sous_domaine = sous_domaines.num_sous_domaine
        ORDER BY domaines.num_domaine';
        $questions = $bdd -> prepare($texte_req);
        $questions -> execute();

        $filename = 'export_questions.txt';

        // Création du fichier
        $file = fopen($filename,"w");

        foreach ($questions as $question){
            fwrite($file,"\n");
            fwrite($file, "// Question : " . $question['num_question'] . "\n");
            fwrite($file, "// Rubrique : " . $question['domaine'] . "\n");
            fwrite($file, "// Contenu : " . $question['sous_domaine'] . "\n");

            if ($question['image'] == NULL) {
                fwrite($file,"[html]" . $question['question'] . "\n{\n");
            } else {
                fwrite($file,"[html]" . $question['question']);
                fwrite($file, "<img class='img-question' src='image_questions/" . $question['image'] . "'>\n{\n");
            }

            if ($question['bonne_reponse'] == 'A') {
                fwrite($file,"=[html] " . $question['reponseA'] . "\n");
            } else {
                fwrite($file,"~[html] " . $question['reponseA'] . "\n");
            }

            if ($question['bonne_reponse'] == 'B') {
                fwrite($file,"=[html] " . $question['reponseB'] . "\n");
            } else {
                fwrite($file,"~[html] " . $question['reponseB'] . "\n");
            }

            if ($question['bonne_reponse'] == 'C') {
                fwrite($file,"=[html] " . $question['reponseC'] . "\n");
            } else {
                fwrite($file,"~[html] " . $question['reponseC'] . "\n");
            }

            if ($question['bonne_reponse'] == 'D') {
                fwrite($file,"=[html] " . $question['reponseD'] . "\n");
            } else {
                fwrite($file,"~[html] " . $question['reponseD'] . "\n");
            }

            fwrite($file,"}\n");


        }

        fclose($file);

        // Téléchargement
        header("Content-Description: File Transfer");
        header("Content-Disposition: attachment; filename=export_questions_gift.txt");
        header("Content-Type: text/plain; ");

        flush();
        readfile($filename);

        // On efface le fichier côté serveur
        unlink($filename);
        exit();

    endif;
