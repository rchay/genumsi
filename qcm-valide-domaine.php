<?php session_start();

include("head.php");

if (empty($_SESSION) or $_SESSION['connecte'] != true) :
    include("header.php");
    echo "Vous ne devriez pas être ici : <a href='index.php'>Retour</a>";
else :

    include("connexionbdd.php");
    include("header.php");
    include("nav.php");

    include "phpqrcode/qrlib.php";

    include("url-qcm.php");


    $req_increment = $bdd->prepare('UPDATE informations_admin SET qcms = qcms + 1 WHERE 1');
    $req_increment->execute();
    // Tableau contenant le nombre questions de chaque domaine (indexé par le domaine)
    $tab_questions_domaine = array();
    $i = 1;
    foreach ($_POST as $key => $quest_par_theme) {
        $tab_questions_domaine[substr($key,  5)] = $quest_par_theme;
        $i++;
    }

    $cle = '';

    foreach ($tab_questions_domaine as $key => $nb) {
        if ($nb > 0) {
            $req_quest = $bdd->prepare("SELECT * FROM questions WHERE num_domaine = ?");
            $req_quest->execute(array($key));

            $tab = $req_quest->fetchAll();

            if ($nb == 1) {
                $cles_aleatoires = array(array_rand($tab, $nb));
            } else if ($nb > 1) {
                $cles_aleatoires = array_rand($tab, $nb);
            }

            foreach ($cles_aleatoires as $cle_aleatoire) {
                $cle .= $tab[$cle_aleatoire]['num_question'] . ';';
            }
        }
    }
    $cle = substr($cle, 0, -1);

    $num_questions = explode(';', $cle);

    $cle64 = base64_encode($cle);

    ?>

    <h1 class='h1-qcm'>Validation du QCM</h1>

    <p>Votre QCM comporte les questions numéros (dans la base données) :</p>

    <ul>

        <?php foreach ($num_questions as $n) : ?>
            <li><?= $n ?></li>

        <?php endforeach ?>

    </ul>

    <div class='choix-qcm'>
        <p>Si ce qcm vous plaît vous pouvez : </p>
        <div class='ul-valide'>
            <form type='get' action='qcm.php' class='form-valide' target="_blank">
                <input type="hidden" name='cle' value='<?= $cle64 ?>'>
                <input type="hidden" name='p' value='<?= base64_encode($_SESSION['num_util']) ?>'>
                <button class='btn btn-info btn-valide' type='submit'>Accéder au QCM complet</button>
            </form>

            <form type='get' action='qcm-print.php' class='form-valide' target="_blank">
                <input type="hidden" name='cle' value='<?= $cle64 ?>'>
                <button class='btn btn-info btn-valide' type='submit'>Accéder au QCM complet à imprimer</button>
            </form>

            <form type='get' action='qcm-corrige.php' target="_blank" class='form-valide'>
                <input type="hidden" name='cle' value='<?= $cle64 ?>'>
                <button class='btn btn-info btn-valide' type='submit'>Accéder au corrigé complet</button>
            </form>

            <p class='p-lien-qcm'>Le partager avec vos élèves à l'aide de ce lien :
                <input type='text' class='lien-qcm' id='lien-qcm' value='<?= url() ?>?cle=<?= $cle64 ?>&p=<?= base64_encode($_SESSION['num_util']) ?>' onclick='this.select() '>
            </p>

            <?php
                $content = url() . "?cle=" . $cle64 . "&p=" . base64_encode($_SESSION['num_util']);

                $filename = 'image_qrcode/qrcode.png';

                $errorCorrectionLevel = 'H';

                $matrixPointSize = 4;

                QRcode::png($content, $filename, $errorCorrectionLevel, $matrixPointSize, 2);

                ?>
            <p class='p-lien-qcm'>Le partager avec vos élèves à l'aide de ce QR-code :</p>
            <img class='qrcode' src="image_qrcode/qrcode.png" alt="" />
        </div>

        <p>Si vous n'êtes pas satisfaits, vous pouvez aussi relancer la sélection aléatoire des questions</p>
        <p>Celle-ci étant choisies au hasard, vous pouvez tout à fait retomber sur les mêmes questions...</p>
        <form method='post' action='qcm-valide-domaine.php' class='form-valide'>
            <?php
                foreach ($_POST as $key => $value) :
                    ?>
                <input type="hidden" name='<?= $key ?>' value='<?= $value ?>'>
            <?php
                endforeach;
                ?>
            <button class='btn btn-info btn-valide' type='submit'>Relancer la sélection</button>
        </form>

    </div>

    <h2>Voici un aperçu de celui-ci. Afin d'alléger la page, les réponses ne sont pas affichées.</h2>

    <?php
        //=====================================QMC inséré
        // Création de la chaîne de caractère (num_question, num_question...) nécessaire
        // à la requête SQL
        $tab_requete = "(";
        foreach ($num_questions as $num) {
            $tab_requete = $tab_requete . $num . ",";
        }

        $tab_requete = substr($tab_requete, 0, -1) . ")";


        // Récupération de toutes les questions correspondants aux numéros du GET
        $texte_req = 'SELECT * FROM questions INNER JOIN domaines ON questions.num_domaine = domaines.num_domaine WHERE num_question IN ' . $tab_requete . ' ORDER BY domaines.num_domaine, num_question ';
        $questions = $bdd->prepare($texte_req);
        $questions->execute();

        $domaine_precedent = '';

        $numero_q = 0;

        ?>

    <section class='qcm'>
        <h1 class='h1-qcm'>QCM de NSI</h1>

        <?php
            // Génération du code html du QCM
            while ($question = $questions->fetch()) :
                if ($question['domaine'] != $domaine_precedent) :

                    ?>
                <h2 class='h2-domaine'><?= $question['domaine'] ?></h2>
            <?php
                        $domaine_precedent = $question['domaine'];
                    endif;
                    $numero_q++;
                    ?>
            <li>
                <b>Question n°<?= $numero_q ?> :</b>

                <?= $question['question'] ?>

                <?php
                        if (!is_null($question['image'])) :
                            ?>

                    <img class='img-question' src="image_questions/<?= $question['image'] ?>">

                <?php endif; ?>


                <div class='reponse-qcm-valide'>
                    <p class='reponses-affiche'>Réponses :</p>
                    <ul type='A' class='reponses-cachees'>
                        <li class='reponse'><?= $question['reponseA'] ?></li>
                        <li class='reponse'><?= $question['reponseB'] ?></li>
                        <li class='reponse'><?= $question['reponseC'] ?></li>
                        <li class='reponse'><?= $question['reponseD'] ?></li>
                    </ul>
                </div>
            </li>
            <br>
        <?php endwhile ?>

    </section>

    <?php ?>
    <div class='choix-qcm'>
        <p>Si ce qcm vous plaît vous pouvez : </p>
        <div class='ul-valide'>

            <form type='get' action='qcm.php' class='form-valide' target="_blank">
                <input type="hidden" name='cle' value='<?= $cle64 ?>'>
                <input type="hidden" name='p' value='<?= base64_encode($_SESSION['num_util']) ?>'>
                <button class='btn btn-info btn-valide' type='submit'>Accéder au QCM complet</button>
            </form>

            <form type='get' action='qcm-print.php' class='form-valide' target="_blank">
                <input type="hidden" name='cle' value='<?= $cle64 ?>'>
                <button class='btn btn-info btn-valide' type='submit'>Accéder au QCM complet à imprimer</button>
            </form>

            <form type='get' target="_blank" action='qcm-corrige.php' class='form-valide'>
                <input type="hidden" name='cle' value='<?= $cle64 ?>'>
                <button class='btn btn-info btn-valide' type='submit'>Accéder au corrigé complet</button>
            </form>

            <p class='p-lien-qcm'>Le partager avec vos élèves à l'aide de ce lien :
                <input type='text' class='lien-qcm' id='lien-qcm' value='<?= url() ?>?cle=<?= $cle64 ?>&p=<?= base64_encode($_SESSION['num_util']) ?>' onclick='this.select() '>
            </p>
            <p class='p-lien-qcm'>Le partager avec vos élèves à l'aide de ce QR-code :</p>
            <img class='qrcode' src="image_qrcode/qrcode.png" alt="" />
        </div>

        <p>Si vous n'êtes pas satisfaits, vous pouvez aussi relancer la sélection aléatoire des questions</p>
        <p>Celle-ci étant choisies au hasard, vous pouvez tout à fait retomber sur les mêmes questions...</p>
        <form method='post' action='qcm-valide-domaine.php' class='form-valide'>
            <?php
                foreach ($_POST as $key => $value) :
                    ?>
                <input type="hidden" name='<?= $key ?>' value='<?= $value ?>'>
            <?php
                endforeach;
                ?>
            <button class='btn btn-info btn-valide' type='submit'>Relancer la sélection</button>
        </form>
    </div>

<?php
endif;

include("footer.php")
?>

</body>

</html>