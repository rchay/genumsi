<?php session_start();
include("head.php");

if (empty($_SESSION) or $_SESSION['connecte'] != true) :
    include("header.php");
    echo "Vous ne devriez pas être ici : <a href='index.php'>Retour</a>";
else :
    include('connexionbdd.php');
    include("header.php");
    include("nav.php");
    ?>

    <h1 class='h1-qcm'>Accueil</h1>

    <p>
        Bienvenue sur le site <b>Genumsi</b>
    </p>

    <p>Ce site vous permet de générer des QCM de NSI en choisissant les questions de façon aléatoire ou selon vos envies !</p>

    <p>L'un des objectifs du site est de centraliser les questions NSI de tous les collègues qui le souhaitent.</p>

    <p>Alors n'hésitez pas à <a href="ajout.php" style='font-weight:bold;color:purple;'>contribuer</a> et à ajouter des questions !</p>

    <p>
        Vous pouvez donc :
    </p>
    <ul id="liste-accueil">
        <li>Créer des qcm pour vos élèves
            <form action="selection-niveau.php" method='POST' id='form-nav'>
                <input hidden value="1" name='niveau' id='hidden-nav'>
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                        <a class="link" href="javascript:lien('1');">Aléatoirement - Niveau 1ère</a>
                    </li>
                    <li class="nav-item">
                        <a class="link" href="javascript:lien('T');">Aléatoirement - Niveau Terminale</a>
                    </li>
                    <li><a class="link" href="selection-manuelle.php">Par sélection manuelle</a></li>
                    <li><a class="link" href="selection-parliste.php">A partir d'une liste</a></li>
                </ul>
            </form>
        </li>
        <li>
            Consulter les résultats de vos élèves
            <ul class="navbar-nav mr-auto">
                <li><a class="link" href="resultats.php">Résultats</a></li>

            </ul>
        </li>
        <li>Ajouter, modifier ou exporter des questions de la base
            <ul class="navbar-nav mr-auto">
                <li><a class="link" href="ajout.php">Ajout</a></li>
                <li><a class="link" href="modification.php">Modification</a></li>
                <li><a class="link" href="exports.php">Export</a></li>
            </ul>
        </li>
    </ul>


<?php
endif;
?>

<?php include("footer.php") ?>

</body>

<script>
    function lien(niveau) {
        $('#hidden-nav').val(niveau);
        $('#form-nav').submit();
    }
</script>


</html>