<?php
include("head.php");
?>

<div id="conteneur-general">

    <?php
    // Connexion à la base de données avec PDO
    include("connexionbdd.php");

    // Récupération de la clé du QCM (au format num_question;num_question...)
    $cle = base64_decode($_GET['cle']);

    // Création de la chaîne de caractère (num_question, num_question...) nécessaire
    // à la requête SQL
    $num_questions = explode(';', $cle);
    $ordres_rep = array();

    $tab_requete = "(";
    $rang = 0;
    foreach ($num_questions as $num) {
        $tab_requete = $tab_requete . $num . ",";
        $ordres_rep[$rang] = "ABCD";
        $rang++;
    }
    $tab_requete = substr($tab_requete, 0, -1) . ")";

    ?>
    <section class='qcm-print'>
        <h1 class='h1-qcm'>QCM de NSI</h1>
        <h4>
            Une bonne réponse rapporte 3 points. Une mauvaise retire 1 point. Une absence de réponse n'est pas pénalisée.
        </h4>

        <p>Nom : </p>

        <p>Prénom : </p>

        <p>Classe : </p>

        <br>
        <?php
        // Récupération de toutes les domaines correspondants aux questions du GET
        $texte_req = 'SELECT questions.num_domaine FROM questions INNER JOIN domaines ON questions.num_domaine = domaines.num_domaine WHERE num_question IN ' . $tab_requete . '  GROUP BY domaines.num_domaine';
        $domaines = $bdd->prepare($texte_req);
        $domaines->execute();

        $domaines = $domaines->fetchAll(PDO::FETCH_ASSOC);

        $domaine_precedent = '';

        $numero_q = 1;

        $num_q = 0;

        foreach ($domaines as $domaine) :


            // Récupération de toutes les questions correspondants aux numéros du GET
            $texte_req = 'SELECT * FROM questions INNER JOIN domaines ON questions.num_domaine = domaines.num_domaine  WHERE num_question IN ' . $tab_requete . ' AND questions.num_domaine = ? ORDER BY num_question';
            $questions = $bdd->prepare($texte_req);
            $questions->execute(array($domaine['num_domaine']));

            $questions = $questions->fetchAll();

            shuffle($questions);

            foreach ($questions as $question) :
                if ($question['domaine'] != $domaine_precedent) :
                    ?>
                    <div class='col-md-12'>
                        <h2 class='h2-domaine'><?= $question['domaine'] ?></h2>
                    </div>
        <?php

                    $domaine_precedent = $question['domaine'];
                endif;

                $ordres_rep[$num_q] = str_shuffle($ordres_rep[$num_q]);
                $num_q++;

                include("question-print.php");
            endforeach;
        endforeach;
        ?>

    </section>

</div>

</body>

</html>